﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Predicto.Models
{
    public class User_information_details
    {

        public string id { get; set; }
        public string login_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email_id { get; set; }
        public string phone_no { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public System.DateTime created_date { get; set; }
    }
}