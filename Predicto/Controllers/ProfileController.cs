﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using Predicto.Models;
using System.Linq;
using Predicto.App_Code;

namespace Predicto.Controllers
{
    public class ProfileController : ApiController
    {
        private PredictoEntities db = new PredictoEntities();
        Global oglobal = new Global();
        public Response Add_User_Information(User_information_details data)
        {
            user_information ostatus = new user_information();

            Response oresponse = new Response();
            var odata = (from g in db.user_information where g.login_id == data.login_id select g).ToArray();
            if (odata.Length == 0)
            {

                ostatus.login_id = data.login_id;
                ostatus.id = Guid.NewGuid().ToString();
                ostatus.firstname = data.firstname;
                ostatus.lastname = data.lastname;
                ostatus.email_id = data.email_id;
                ostatus.phone_no = data.phone_no;
                ostatus.created_by = data.created_by;
                ostatus.modified_by = data.modified_by;
                ostatus.created_date = data.created_date;
                ostatus.modified_date = data.modified_date;

                db.user_information.Add(ostatus);
                db.SaveChanges();

                oresponse.status = 1;
                oresponse.message = "Inserted Successfull";
                oresponse.content = "";
                return oresponse;
            }
            else
            {
                foreach (var item in odata)
                {
                    item.lastname = data.lastname;
                    item.firstname = data.firstname;
                    item.email_id = data.email_id;
                    item.phone_no = data.phone_no;
                }

                db.SaveChanges();
                oresponse.status = 1;
                oresponse.message = "Updated Successfull";
                oresponse.content = "";
                return oresponse;
            }
        }
        public Response Get_User_Details(string Login_id)
        {
            var odata = from user in db.user_information
                        where user.login_id == Login_id
                        select new User_information_details
                        {
                            login_id = user.login_id,
                            created_date = user.created_date,
                            email_id = user.email_id,
                            firstname = user.firstname,
                            lastname = user.lastname,
                            phone_no = user.phone_no,
                            id = user.id,
                            created_by=user.created_by,
                            modified_by=user.modified_by,
                            modified_date=user.modified_date,
                           
                        };
            
            return oglobal.Success(odata);
        }

        

        public Response Change_Password(string Login_id, string old_password,string new_password)
        {
            user_information ostatus = new user_information();

            Response oresponse = new Response();
            var odata = (from g in db.user_login where g.login_id == Login_id && g.password == old_password select g).ToArray();
            if (odata.Length == 0)
            {
                 
                oresponse.status = 0;
                oresponse.message = "password is not matched";
                oresponse.content = "";
                return oresponse;
            }
            else
            {
                foreach (var item in odata)
                {
                    item.password = new_password;
                }

                db.SaveChanges();
                oresponse.status = 1;
                oresponse.message = "Updated Successfull";
                oresponse.content = "";
                return oresponse;
            }
        }

    }
}