﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Predicto.Models
{
    public class Validation_Status
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
    }

    public class Campaign_Validation_Stage
    {
        public string campaign_id { get; set; }
        public int stage { get; set; }
        public bool? IsFailure { get; set; }
        public string validation_message { get; set; }
        public Validation_Status[] ostatus { get; set; }
    }
}