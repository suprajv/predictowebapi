﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Predicto.Models;
using Predicto.App_Code;
using System.Data.Entity;

namespace Predicto.Controllers
{
    public class masterController : ApiController
    {
        private PredictoEntities db = new PredictoEntities();
        Global oglobal = new Global();

        public Response GetValidation_status()
        {
            var oStatus_List = (from l in db.master_validation_stages where l.Isdeleted==Global.NotDeleted
                                select new Validation_Status
                                {
                                    Id = l.id,
                                    Name = l.name
                                }).ToArray();
            return oglobal.Success(oStatus_List);
        }

        public Campaign_Validation_Stage Get_Campaign_stages(string campaign_Id)
        {
            Response obj = new Response();
            var q1 = (from c in db.upload_manager where c.scoring_id != null where c.campaign_id==campaign_Id select c.id).FirstOrDefault(); 
            if (q1 == null)
            {
                var oStatus_List = (from l in db.master_validation_stages
                                    where l.Isdeleted == Global.NotDeleted
                                    select new Validation_Status
                                    {
                                        Id = l.id,
                                        Name = l.name
                                    }).ToArray();

                var q2 = (from c in db.campaign_validation_stage where c.campaign_id==campaign_Id
                          select new Campaign_Validation_Stage {
                              campaign_id=c.campaign_id,
                              IsFailure=c.is_failure,
                              stage=c.validation_status,
                              validation_message=c.validation_message
                          }).FirstOrDefault();

                q2.ostatus = oStatus_List;

                return q2;
            }
            else
            {

                var oStatus_List = (from l in db.master_scoring_stages
                                    where l.Isdeleted == Global.NotDeleted
                                    select new Validation_Status
                                    {
                                        Id = l.id,
                                        Name = l.name
                                    }).ToArray();

                var q2 = (from c in db.campaign_score_stage where c.campaign_id == campaign_Id
                          select new Campaign_Validation_Stage
                          {
                              campaign_id = c.campaign_id,
                              IsFailure = c.is_failure,
                              stage = c.scoring_status,
                              validation_message = c.scoring_message
                          }).FirstOrDefault();
                q2.ostatus = oStatus_List;
                return q2;
            }
        }

        public Response Add_Validation_status(string Name,string Id)
        {
            Response oresponse = new Response();
            int step = Convert.ToInt32(Id);
            var odata = (from g in db.master_validation_stages where g.id == step && g.name==Name select g).ToArray();
            if (odata.Length == 0)
            {
                master_validation_stages ostatus = new master_validation_stages();
                ostatus.id = step;
                ostatus.name = Name;
                ostatus.Isdeleted = false;
                ostatus.created_date = DateTime.Now;
                db.master_validation_stages.Add(ostatus);
                db.SaveChanges();

                oresponse.status = 1;
                oresponse.message = "Inserted Successfull";
                oresponse.content = "";
                return oresponse;
            }
            else
            {
                oresponse.status = 0;
                oresponse.message = "Status already exist";
                oresponse.content = "";
                return oresponse;
            }
        }

        public Response Get_Document_Types()
        {
            var oStatus_List = (from l in db.master_document_type where l.isdeleted==Global.NotDeleted
                                select new Document_Type
                                {
                                    Id = l.id,
                                    Name = l.name
                                }).ToArray();

            return oglobal.Success(oStatus_List);
        }

        public Response Add_Document_Type(string Name)
        {
            Response oresponse = new Response();
            var odata = (from g in db.master_document_type where g.name == Name select g).ToArray();
            if (odata.Length == 0)
            {
                master_document_type otype = new master_document_type();
                otype.name = Name;
                otype.created_date = DateTime.Now;
                otype.isdeleted = false;
                otype.id = Guid.NewGuid().ToString();
                otype.status_id = 1;
                otype.description = "";
                db.master_document_type.Add(otype);
                db.SaveChanges();

                oresponse.status = 1;
                oresponse.message = "Inserted Successfull";
                oresponse.content = "";
                return oresponse;
            }
            else
            {
                oresponse.status = 0;
                oresponse.message = "Type already exist";
                oresponse.content = "";
                return oresponse;
            }
        }

        public Response Get_Experiment_Types()
        {
            var oStatus_List = (from l in db.master_experiment_type where l.isdeleted==Global.NotDeleted
                                select new Experiment_Type
                                {
                                    Id = l.id,
                                    Name = l.name,
                                    Description = l.description
                                }).ToArray();

            return oglobal.Success(oStatus_List);
        }

        public Response Add_Experiment_Type(Create_Experiment_Type odata)
        {
            Response oresponse = new Response();
            var q1 = (from g in db.master_experiment_type where g.name == odata.Name select g).ToArray();
            if (q1.Length == 0)
            {
                master_experiment_type otype = new master_experiment_type();
                otype.name = odata.Name;
                otype.description = odata.Description;
                otype.created_date = DateTime.Now;
                otype.created_by = "";
                otype.modified_date = DateTime.Now;
                otype.modified_by = "";
                otype.isdeleted = false;
                otype.id = Guid.NewGuid().ToString();
                otype.status_id = 1;
                db.master_experiment_type.Add(otype);

                foreach(var item in odata.Document_id)
                {
                    document_experiment_map omap = new document_experiment_map();
                    omap.id = Guid.NewGuid().ToString();
                    omap.status_id = 1;
                    omap.isdeleted = false;
                    omap.document_type_id = item;
                    omap.experiment_tye_id = otype.id;
                    omap.created_by = "";
                    omap.created_date = DateTime.Now;
                    omap.modified_by = "";
                    omap.modified_date = DateTime.Now;
                    db.document_experiment_map.Add(omap);
                }
                db.SaveChanges();

                oresponse.status = 1;
                oresponse.message = "Inserted Successfull";
                oresponse.content = "";
                return oresponse;
            }
            else
            {
                oresponse.status = 0;
                oresponse.message = "Type already exist";
                oresponse.content = "";
                return oresponse;
            }
        }

        public Response Get_Admin_List(string departmentId)
        {
            var q1 = (from l in db.user_login where l.dept_id == departmentId  select new Login_Response
            {
                user_id=l.Exp_id.ToString(),
                user_name=l.user_id
            }).ToArray();
            if (q1 != null)
            {
                return oglobal.Success(q1);
            }
            else
            {
                return oglobal.InvalidInput();
            }
        }

        public Response Add_New_Admin(Create_admin odata)
        {
            Response oresponse = new Response();
            var q1 = (from g in db.user_login where g.user_id == odata.user_id select g).ToArray();
            if (q1.Length == 0)
            {
                user_login ologin=new user_login();
                ologin.login_id = Guid.NewGuid().ToString();
                ologin.user_id = odata.user_id;
                ologin.password = odata.password;
                ologin.role_id = 2;
                ologin.status_id = 1;
                ologin.dept_id = odata.department_id;
                ologin.created_date = DateTime.Now;
                ologin.created_by = "";
                ologin.modified_date = DateTime.Now;
                ologin.modified_by = "";
                db.user_login.Add(ologin);
                db.SaveChanges();

                oresponse.status = 1;
                oresponse.message = "Inserted Successfull";
                oresponse.content = "";
                return oresponse;
            }
            else
            {
                oresponse.status = 0;
                oresponse.message = "user Id already exist";
                oresponse.content = "";
                return oresponse;
            }
        }

        public Response Get_Department_List()
        {
            var q1 = (from d in db.master_departments where d.isdeleted==Global.NotDeleted select new Departments_List
            {
                Id=d.dept_id,
                name=d.name,
                details=d.details
            }).ToArray();
            if (q1 != null)
            {
                return oglobal.Success(q1);
            }
            else
            {
                return oglobal.InvalidInput();
            }
        }

        public Response Add_New_Department(Create_Department odata)
        {
            Response oresponse = new Response();
            var q1 = (from g in db.master_departments where g.name == odata.name select g).FirstOrDefault();
            if (q1 ==null)
            {
                master_departments odep = new master_departments();
                odep.dept_id = Guid.NewGuid().ToString();
                odep.name = odata.name;
                odep.details = odata.details;
                odep.isdeleted = false;
                odep.status_id = 1;
                odep.created_date = DateTime.Now;
                odep.created_by = "";
                odep.modified_date = DateTime.Now;
                odep.modified_by = "";
                db.master_departments.Add(odep);
                db.SaveChanges();

                oresponse.status = 1;
                oresponse.message = "Inserted Successfull";
                oresponse.content = "";
                return oresponse;
            }
            else
            {
                oresponse.status = 0;
                oresponse.message = "Department name already exist";
                oresponse.content = "";
                return oresponse;
            }
        }

        [HttpDelete]
        public Response Experiment_Delete(string Experiment_id)
        {
            try
            {
                var query = (from e in db.master_experiment_type where e.id == Experiment_id select e).FirstOrDefault();

                if (query != null)
                {
                    db.master_experiment_type.Remove(query);
                    db.SaveChanges();
                    return oglobal.Success(string.Empty);
                }
                else
                {
                    return oglobal.Failure();
                }
            }
            catch (Exception e)
            {
                return oglobal.Failure();
            }
        }

        [HttpDelete]
        public Response Document_Delete(string document_id)
        {
            try
            {
                var query = (from e in db.master_document_type where e.id == document_id select e).FirstOrDefault();

                if (query != null)
                {
                    db.master_document_type.Remove(query);
                    db.SaveChanges();
                    return oglobal.Success(string.Empty);
                }
                else
                {
                    return oglobal.Failure();
                }
            }
            catch (Exception e)
            {
                return oglobal.Failure();
            }
        }

        [HttpDelete]
        public Response department_Delete(string department_id)
        {
            try
            {
                var query = (from e in db.master_departments where e.dept_id == department_id select e).FirstOrDefault();

                if (query != null)
                {
                    db.master_departments.Remove(query);
                    db.SaveChanges();
                    return oglobal.Success(string.Empty);
                }
                else
                {
                    return oglobal.Failure();
                }
            }
            catch (Exception e)
            {
                return oglobal.Failure();
            }
        }

        [HttpDelete]
        public Response Validation_Delete(int validation_id)
        {
            try
            {
                var query = (from e in db.master_validation_stages where e.id == validation_id select e).FirstOrDefault();

                if (query != null)
                {
                    db.master_validation_stages.Remove(query);
                    db.SaveChanges();
                    return oglobal.Success(string.Empty);
                }
                else
                {
                    return oglobal.Failure();
                }
            }
            catch (Exception e)
            {
                return oglobal.Failure();
            }
        }
    }
}
