﻿using Predicto.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Xml;

namespace Predicto.App_Code
{
    public class Global
    {
        public static string SQL_CONN_STRING = Convert.ToString(ConfigurationManager.AppSettings["SQL_CONN_STRING"]);

        public static bool NotDeleted = false;
        public static bool deleted = true;
        public static SqlConnection GetConnection()
        {
            SqlConnection dbconnection = new SqlConnection(Global.SQL_CONN_STRING);
            try
            {
                dbconnection.Open();
                return dbconnection;
            }
            catch (System.InvalidOperationException e)
            {
                Console.Write(e);
                return null;
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                Console.Write(e);
                return null;
            }
        }

        #region Responses

        public Response Failure()
        {
            Response obj = new Response();
            obj.status = 0;
            obj.message = "Failed to process Request";
            obj.content = "";
            return obj;
        }

        public Response Success(dynamic content)
        {
            Response obj = new Response();
            obj.status = 1;
            obj.message = "success";
            obj.content = content;
            return obj;
        }

        public Response Success()
        {
            Response obj = new Response();
            obj.status = 2;
            obj.message = "No Data has been Found";
            obj.content = "";
            return obj;
        }

        public Response DuplicateFound()
        {
            Response obj = new Response();
            obj.status = 3;
            obj.message = "Data Already Present";
            obj.content = "";
            return obj;
        }

        public Response InvalidInput()
        {
            Response obj = new Response();
            obj.status = 4;
            obj.message = "Invalid Input Details";
            obj.content = "";
            return obj;
        }

        #endregion
    }
}