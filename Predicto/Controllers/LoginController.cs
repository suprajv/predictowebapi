﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using Predicto.Models;
using System.Linq;
using Predicto.App_Code;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace Predicto.Controllers
{
    public class LoginController : ApiController
    {
        private PredictoEntities db = new PredictoEntities();
        Global oglobal = new Global();

        public Response UserLogin(Login odata)
        {
            var q1 = (from l in db.user_login
                      where l.user_id == odata.User_Id & l.password == odata.Password
                      select new Login_Response
                      {
                          user_id = l.login_id,
                          user_name = l.user_id,
                          role_id = l.role_id
                      }).FirstOrDefault();
            if (q1 != null)
            {
                return oglobal.Success(q1);
            }
            else if (odata.User_Id == "superadmin" && odata.Password == "admin")
            {

                var obj = new Login_Response
                {
                    user_id = "123",
                    user_name = "Super Admin",
                    role_id = 1
                };
                return oglobal.Success(obj);
            }
            else
            {
                return oglobal.InvalidInput();
            }
        }

        public Response GetUser_Details(string Login_Id)
        {
            var q1 = (from u in db.user_information where u.login_id == Login_Id select u).FirstOrDefault();
            if (q1 != null)
            {
                return oglobal.Success(q1);
            }
            else
            {
                return oglobal.InvalidInput();
            }
        }


        public Response Get_Dashboard_Details(string user_id)
        {
            Dashboard odash = new Dashboard();
            var q1 = (from g in db.campains
                      join t in db.transactions on g.campain_id equals t.campain_id
                      join e in db.master_experiment_type on g.experiment_type_id equals e.id
                      where g.created_by == user_id & g.status_id != 2
                      select new Campain_count
                      {
                          validation_status = g.status_id.ToString(),
                          status = g.status_id
                      }).ToArray();
            odash.odata = Get_Data();
            odash.odetails = q1;
            if (odash.odata != null)
            {
                odash.otarget = odash.odata.Select(x => x.target).Distinct().ToArray();
                odash.ocolumn = odash.odata.Select(x => x.column).Distinct().ToArray();
            }
            else
            {
                odash.otarget = null;
                odash.ocolumn = null;

            }
            if (q1 != null)
            {
                return oglobal.Success(odash);
            }
            else
            {
                return oglobal.InvalidInput();
            }
        }

        public Visual_Data[] Get_Data()
        {
            List<Visual_Data> olist = new List<Visual_Data>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
               SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT ISNULL(name,''),ISNULL([FEATURE],''),ISNULL([BIN],''),ISNULL([freequency],0) as freequency FROM [BankingSolution].[dbo].[DSPL_BivariateSummary_View] inner join  [predictoFEND].[dbo].[campains] on [campaignID]=campain_id");

                while (reader.Read())
                {
                    olist.Add(new Visual_Data(
                        reader.GetString(0),
                        reader.GetString(1),
                        reader.GetString(2),
                        reader.GetDouble(3).ToString()));
                }
                reader.Close();
                return olist.ToArray();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }

        }

        public Visual_Data[] Get_Sorted_Result(string target, string column, int type)
        {
            List<Visual_Data> olist = new List<Visual_Data>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
                if (type == 1)
                {
                    SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT ISNULL(name,''),ISNULL([FEATURE],''),ISNULL([BIN],''),ISNULL(round([freequency_prec],2),0) as freequency FROM [BankingSolution].[dbo].[DSPL_BivariateSummary_View] inner join  [predictoFEND].[dbo].[campains] on [campaignID]=campain_id  where name='" + target + "' and FEATURE='" + column + "' order by BIN desc");


                    while (reader.Read())
                    {
                        olist.Add(new Visual_Data(
                            reader.GetString(0),
                            reader.GetString(1),
                            reader.GetString(2),
                            reader.GetDouble(3).ToString()));
                    }
                    reader.Close();
                    return olist.ToArray();
                }

                else
                {
                    SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT ISNULL(name,''),ISNULL([FEATURE],''),ISNULL([BIN],''),ISNULL(round([freequency],2),0) as freequency FROM [BankingSolution].[dbo].[DSPL_BivariateSummary_View] inner join  [predictoFEND].[dbo].[campains] on [campaignID]=campain_id  where name='" + target + "' and FEATURE='" + column + "' order by BIN desc");
                    while (reader.Read())
                    {
                        olist.Add(new Visual_Data(
                            reader.GetString(0),
                            reader.GetString(1),
                            reader.GetString(2),
                            reader.GetDouble(3).ToString()));
                    }
                    reader.Close();
                    return olist.ToArray();
                }
            }

            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }
        }

        public Line_Data[] Get_LR_Result()
        {
            List<Line_Data> olist = new List<Line_Data>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
                SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT [Bucket],[Actual],[predicted] FROM [BankingSolution].[dbo].[LR_Efficiency] where [Bucket]='Low' union all SELECT [Bucket],[Actual],[predicted] FROM [BankingSolution].[dbo].[LR_Efficiency] where [Bucket]='Medium' union all SELECT [Bucket],[Actual],[predicted] FROM [BankingSolution].[dbo].[LR_Efficiency] where [Bucket]='High'");

                while (reader.Read())
                {
                    olist.Add(new Line_Data(
                        reader.GetString(0),
                        Convert.ToDecimal(reader.GetString(1)),
                        Convert.ToDecimal(reader.GetString(2))));
                }
                reader.Close();
                return olist.ToArray();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }
        }

        public Line_Data[] Get_NN_Result()
        {
            if (1 != 1)
            {
                List<Visual_Data> olist = new List<Visual_Data>();
                SqlConnection connection = null;
                try
                {
                    try
                    {
                        connection = Global.GetConnection();
                    }
                    catch (System.Exception e)
                    {
                        Console.Write(e);
                        return null;
                    }
                    SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT [targetVariable],[columnName],[binValue],ISNULL([freequency],0) as freequency FROM [BankingSolution].[dbo].[DSPL_BivariateSummary_View] order by binValue desc");

                    while (reader.Read())
                    {
                        olist.Add(new Visual_Data(
                            reader.GetString(0),
                            reader.GetString(1),
                            reader.GetString(2),
                            reader.GetDouble(3).ToString()));
                    }
                    reader.Close();
                    return null;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    return null;
                }
                finally
                {
                    if (connection != null)
                        connection.Dispose();
                }
            }
            else
            {
                List<Line_Data> odata = new List<Line_Data>();
                odata.Add(new Line_Data { bucket = "Low", actual = 4, predicted = 5 });
                odata.Add(new Line_Data { bucket = "Medium", actual = 12, predicted = 10 });
                odata.Add(new Line_Data { bucket = "High", actual = 19, predicted = 20 });

                return odata.ToArray();
            }
        }


        public Line_Data[] Get_Bubble_Result(string filter)
        {
            List<Line_Data> olist = new List<Line_Data>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
                SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT [Probability Bucket],[ColumnName],[Value] FROM [BankingSolution].[dbo].[Customer_Segments] where ColumnName='"+ filter +"'");

                while (reader.Read())
                {
                    olist.Add(new Line_Data(
                         reader.GetString(0),
                         0,
                         Convert.ToDecimal(reader.GetString(2))));
                }
                reader.Close();
                return olist.ToArray();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }
        }


        public Bubble_Filters Get_Bubble_Filters()
        {
            Bubble_Filters obj = new Bubble_Filters();
            List<string> olist = new List<string>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
                SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT distinct ColumnName FROM [BankingSolution].[dbo].[Customer_Segments]");

                while (reader.Read())
                {
                    olist.Add(reader.GetString(0));
                }
                reader.Close();
                obj.ofilters = olist.ToArray();
                return obj;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }
        }
    }
}
