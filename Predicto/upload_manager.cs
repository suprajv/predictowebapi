//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Predicto
{
    using System;
    using System.Collections.Generic;
    
    public partial class upload_manager
    {
        public string id { get; set; }
        public int exp_id { get; set; }
        public string file_name { get; set; }
        public string file_path { get; set; }
        public Nullable<System.DateTime> submitted_date { get; set; }
        public Nullable<System.DateTime> Processed_date { get; set; }
        public string campaign_id { get; set; }
        public string scoring_id { get; set; }
        public string document_type_id { get; set; }
        public int validation_status { get; set; }
        public Nullable<bool> is_failure { get; set; }
        public string validation_message { get; set; }
        public bool is_deleted { get; set; }
        public string modified_by { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> modified_date { get; set; }
        public System.DateTime created_date { get; set; }
    
        public virtual master_document_type master_document_type { get; set; }
    }
}
