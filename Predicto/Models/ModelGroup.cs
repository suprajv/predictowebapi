﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Predicto.Models
{
    public class Response
    {
        public int status { get; set; }

        public string message { get; set; }

        public dynamic content { get; set; }
    }

    public class Document_Type
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }

    public class Experiment_Type
    {
        public string Id { get; set; }

        public string Name { get; set; }
        
        public string Description { get; set; }
    }

    public class Create_Experiment_Type
    {
        public string Name { get; set; }
        
        public string Description { get; set; }

        public List<string> Document_id { get; set; }
    }

    public class Create_Campain
    {

        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Experiment_type_id { get; set; }

        public string user_id { get; set; }

    }

    public class Create_transaction
    {
        public string Id { get; set; }

        public string source_type { get; set; }

        public string target_type { get; set; }

        public string campain_id { get; set; }
    }

    public class Create_admin
    {
        public string user_id { get; set; }

        public string department_id { get; set; }

        public string password { get; set; }
    }

    public class Create_Department
    {
        public string name { get; set; }

        public string details { get; set; }
    }
    public class Departments_List
    {
        public string Id { get; set; }

        public string name { get; set; }

        public string details { get; set; }
      
    }
    public class Login_Response
    {
        public string user_id { get; set; }

        public string user_name { get; set; }

        public int role_id { get; set; }
    }

    public class Add_Documents
    {
        public string campaign_id { get; set; }

        public List<Documents_type> odoc { get; set; }    
    }

    public class Documents_type
    {
        public string file_name { get; set; }

        public string file_path { get; set; }

        public string document_type_id { get; set; }
    }

    public class Campain_Details
    {
        
        public string Id { get; set; }

        public string Exp_Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Experiment_type { get; set; }

        public string Transaction_Id { get; set; }

        public string validation_status { get; set; }

        public DateTime started_date { get; set; }

    }

    public class Campain_Complete_Details
    {
        public string Id { get; set; }

        public string Exp_Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Experiment_type { get; set; }

        public string Source_Type { get; set; }

        public string Destination_Type { get; set; }

        public string Validation_stage { get; set; }

        public List<Doc_Info> Documents_List { get; set; }
        
        public List<string> Category_List { get; set; }

        public bool Is_Scoring { get; set; }

    }

    public class Doc_Info
    {
        public string type { get; set; }
        public string name { get; set; }
    }
    public class Campaign_Validaton_Details
    {
        public string Campaign_id { get; set; }
        
        public int validation_status { get; set; }

        public bool? Isfailure { get; set; }

        public string validation_message { get; set; }
    }

    public class Visual_Data
    {
        public Visual_Data()
        {

        }

        public string target { get; set; }

        public string column { get; set; }

        public string bin { get; set; }

        public string frequency { get; set; }

        public Visual_Data(
            string target,
            string column,
            string bin,
            string frequency
            )
        {
            this.target = target;
            this.column = column;
            this.bin = bin;
            this.frequency = frequency;
        }
    }

    public class Line_Data
    {
        public Line_Data()
        {

        }

        public string bucket { get; set; }

        public decimal actual { get; set; }

        public decimal predicted { get; set; }

        public Line_Data(
            string bucket,
            decimal actual,
            decimal predicted
            )
        {
            this.bucket = bucket;
            this.actual = actual;
            this.predicted = predicted;
        }
    }

    public class Dashboard
    {
        public Visual_Data[] odata { get; set; }

        public Campain_count[] odetails { get; set; }

        public string[] ocolumn { get; set; }

        public string[] otarget { get; set; }

        public string ocolumn_id { get; set; }

        public string otarget_id { get; set; }
    }

    public class Campain_count
    {
        public string validation_status { get; set; }

        public int status { get; set; }
    }


    public class Bubble_Data
    {
        public Bubble_Data()
        {

        }

        public string bucket { get; set; }

        public string column { get; set; }

        public int value { get; set; }

        public Bubble_Data(
            string bucket,
            string column,
            int value
            )
        {
            this.bucket = bucket;
            this.column = column;
            this.value = value;
        }
    }

    public class Bubble_Filters
    {
        public string[] ofilters { get; set; }

        public string selected_filter { get; set; }
        
    }

    public class DataHealth
    {
        public string id { get; set; }
        public string category { get; set; }
        public List<Documents> Documents_paths { get; set; }
    }

    public class Documents
    {
        public string File_path { get; set; }

    }

    public class ScoringData
    {
        public string CIF { get; set; }
        public string TITLE { get; set; }
        public string HOME_BRANCH { get; set; }
        public string HOST_ID { get; set; }
        public string NATIONALITY { get; set; }

        public string GENDER { get; set; }
        public string BIRTH_DT { get; set; }
        public string CID_BD { get; set; }
        public string AGE { get; set; }
        public string CUST_CREATION_DATE { get; set; }
        public string RELSHIP_YEARS { get; set; }
        public string VIP_CD { get; set; }
        public string VIP_DESC { get; set; }
        public string EMPL_CATEGORY { get; set; }
        public string HAS_CARD { get; set; }
        public string NBR_CARDS { get; set; }
        public string HAS_LOAN { get; set; }
        public string LOANS_ORGAMT { get; set; }
        public string LOANS_VAL { get; set; }

        public string NBR_LOANS { get; set; }
        public string CUST_TYPE { get; set; }
        public string CUST_SEGMENT { get; set; }
        public string PROFESSION { get; set; }
        public string HAS_FUND { get; set; }
        public string NBR_FUNDS { get; set; }
        public string FUND_VAL { get; set; }
        public string CUST_DEP_VAL { get; set; }
        public string CUST_SELECTION { get; set; }
        public string INCOME { get; set; }
        public string prediction { get; set; }

    }

    public class cross_sell
    {
        public string month { get; set; }
        public string percent { get; set; }
        public string source { get; set; }
        public string destination { get; set; }
    }

    public class summary_stats
    {
        public string month { get; set; }
        public string trans_type { get; set; }
        public string property { get; set; }
        public string property_value { get; set; }
        public string rolling_mean { get; set; }
        public string rolling_std_dev { get; set; }
        public string calc_value { get; set; }
    }
}