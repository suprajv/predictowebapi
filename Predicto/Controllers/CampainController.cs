﻿using Microsoft.ApplicationBlocks.Data;
using Predicto.App_Code;
using Predicto.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Predicto.Controllers
{
    public class CampainController : ApiController
    {
        private PredictoEntities db = new PredictoEntities();
        Global oglobal = new Global();

        public Response Create_New_Campain(Create_Campain odata)
        {
            
            Response oresponse = new Response();
            var q1 = (from g in db.campains where g.campain_id == odata.Id select g).FirstOrDefault();
            if (q1 == null)
            {
                campain obj = new campain();
                obj.name = odata.Name;
                obj.campain_id = odata.Id;
                obj.isdeleted = false;
                obj.start_datetime = DateTime.Now;
                obj.status_id = 2;
                obj.experiment_type_id = odata.Experiment_type_id;
                obj.created_by = odata.user_id;
                obj.description = odata.Description;
                db.campains.Add(obj);
                db.SaveChanges();

                oresponse.status = 1;
                oresponse.message = "Inserted Successfull";
                oresponse.content = "";
                return oresponse;
            }
            else
            {
                q1.description = odata.Description;
                q1.name = odata.Name;
                q1.experiment_type_id = odata.Experiment_type_id;
                db.campains.Attach(q1);
                db.Entry(q1).State = EntityState.Modified;
                db.SaveChanges();

                oresponse.status = 1;
                oresponse.message = "Updated Successfully";
                oresponse.content = "";
                return oresponse;
            }
        }

        public Response Add_Product_hierarchy(string file_path, string login_id)
        {
            Response oresponse = new Response();
            hierarchy_files obj = new hierarchy_files();
            obj.id = Guid.NewGuid().ToString();
            obj.file_path = file_path;
            obj.created_date = DateTime.Now;
            obj.login_id = login_id;
            db.hierarchy_files.Add(obj);
            db.SaveChanges();

            oresponse.status = 1;
            oresponse.message = "Inserted Successfull";
            oresponse.content = "";
            return oresponse;
        }

        public Response Add_transaction_type(Create_transaction odata)
        {
            try
            {
                var q1 = (from g in db.transactions where g.trans_id == odata.Id select g).FirstOrDefault();
                if (q1 == null)
                {
                    Response oresponse = new Response();
                    transaction obj = new transaction();
                    obj.trans_id = odata.Id;
                    obj.isdeleted = false;
                    obj.created_date = DateTime.Now;
                    obj.source_type = odata.source_type;
                    obj.destination_type = odata.target_type;
                    obj.status_id = 1;
                    obj.campain_id = odata.campain_id;
                    obj.created_by = "";
                    db.transactions.Add(obj);
                    db.SaveChanges();

                    oresponse.status = 1;
                    oresponse.message = "Inserted Successfull";
                    oresponse.content = "";
                    return oresponse;
                }
                else
                {

                    Response oresponse = new Response();
                    q1.source_type = odata.source_type;
                    q1.destination_type = odata.target_type;
                    db.transactions.Attach(q1);
                    db.Entry(q1).State = EntityState.Modified;
                    db.SaveChanges();

                    oresponse.status = 1;
                    oresponse.message = "Updated Successfull";
                    oresponse.content = "";
                    return oresponse;
                }
            }
            catch(Exception ex)
            {
                return oglobal.InvalidInput();
            }
        }

        public Response Add_Campain_Documents(Add_Documents odata)
        {
            try
            {
                foreach (var item in odata.odoc)
                {
                    upload_manager obj = new upload_manager();

                    obj.id = Guid.NewGuid().ToString();
                    obj.file_name = item.file_name;
                    obj.file_path = item.file_path;
                    obj.document_type_id = item.document_type_id;
                    obj.is_deleted = false;
                    obj.submitted_date = DateTime.Now;
                    obj.campaign_id = odata.campaign_id;
                    obj.validation_status = 0;
                    obj.is_failure = false;
                    obj.created_date = DateTime.Now;
                    obj.created_by = "";
                    db.upload_manager.Add(obj);
                }
                var q1 = (from g in db.campains join t in db.transactions on g.campain_id equals odata.campaign_id select g).FirstOrDefault();
                if(q1!=null)
                {
                    q1.status_id = 1;
                    db.campains.Attach(q1);
                    db.Entry(q1).State = EntityState.Modified;

                    campaign_validation_stage ostage = new campaign_validation_stage();
                    ostage.id = Guid.NewGuid().ToString();
                    ostage.is_failure = false;
                    ostage.created_date = DateTime.Now;
                    ostage.campaign_id = q1.campain_id;
                    ostage.validation_status = 1;
                    db.campaign_validation_stage.Add(ostage);
                }
                db.SaveChanges();
              
                Response oresponse = new Response();
                oresponse.status = 1;
                oresponse.message = "Inserted Successfull";
                oresponse.content = "";
                return oresponse;
            }
            catch (Exception ex)
            {
                return oglobal.InvalidInput();
            }
        }

        public Response Add_scoring_Documents(Add_Documents odata)
        {
            try
            {
                campaign_scoring oscore = new campaign_scoring();
                oscore.scoring_id = Guid.NewGuid().ToString();
                oscore.campain_id = odata.campaign_id;
                oscore.created_by = string.Empty;
                oscore.created_date = DateTime.Now;
                oscore.isdeleted = false;
                oscore.status_id = 1;
                db.campaign_scoring.Add(oscore);
                foreach (var item in odata.odoc)
                {
                    upload_manager obj = new upload_manager();

                    obj.id = Guid.NewGuid().ToString();
                    obj.file_name = item.file_name;
                    obj.file_path = item.file_path;
                    obj.document_type_id = item.document_type_id;
                    obj.is_deleted = false;
                    obj.submitted_date = DateTime.Now;
                    obj.campaign_id = odata.campaign_id;
                    obj.scoring_id = oscore.scoring_id;
                    obj.validation_status = 0;
                    obj.is_failure = false;
                    obj.created_date = DateTime.Now;
                    obj.created_by = "";
                    db.upload_manager.Add(obj);
                }
                var q1 = (from g in db.campains join t in db.transactions on g.campain_id equals odata.campaign_id select g).FirstOrDefault();
                if (q1 != null)
                {
                    q1.status_id = 1;
                    db.campains.Attach(q1);
                    db.Entry(q1).State = EntityState.Modified;

                    campaign_score_stage ostage = new campaign_score_stage();
                    ostage.id = Guid.NewGuid().ToString();
                    ostage.is_failure = false;
                    ostage.created_date = DateTime.Now;
                    ostage.campaign_id = q1.campain_id;
                    ostage.scoring_status = 1;
                    db.campaign_score_stage.Add(ostage);
                }
                db.SaveChanges();

                Response oresponse = new Response();
                oresponse.status = 1;
                oresponse.message = "Inserted Successfull";
                oresponse.content = "";
                return oresponse;
            }
            catch (Exception ex)
            {
                return oglobal.InvalidInput();
            }
        }

        public Response Select_All_Campains(string user_id)
        {
            Response oresponse = new Response();
            var q1 = (from g in db.campains
                      join t in db.transactions on g.campain_id equals t.campain_id
                      join e in db.master_experiment_type on g.experiment_type_id equals e.id
                      join c in db.campaign_validation_stage on g.campain_id equals c.campaign_id
                      where g.created_by == user_id & g.status_id != 2
                      select new Campain_Details
                      {
                          Id = g.campain_id,
                          Exp_Id = g.exp_id.ToString(),
                          Description = g.description,
                          Experiment_type = e.name,
                          Name = g.name,
                          Transaction_Id = t.trans_id,
                          validation_status = c.validation_status.ToString(),
                          started_date = g.start_datetime
                      }).ToArray();

            oresponse.status = 1;
            oresponse.message = "";
            oresponse.content = q1;
            return oresponse;
        }

        [HttpGet]
        public Response Select_Campain_Details(string campaign_id)
        {
            Response oresponse = new Response();
            var q1 = (from g in db.campains
                      join t in db.transactions on g.campain_id equals t.campain_id
                      join e in db.master_experiment_type on g.experiment_type_id equals e.id
                      where g.campain_id == campaign_id & g.status_id != 2
                      select new Campain_Complete_Details
                      {
                          Id = g.campain_id,
                          Exp_Id = g.exp_id.ToString(),
                          Description = g.description,
                          Experiment_type = e.name,
                          Name = g.name,
                          Destination_Type = t.destination_type,
                          Source_Type = t.source_type
                      }).FirstOrDefault();

            if(q1!=null)
            {
                var q2 = (from u in db.upload_manager join c in db.campains on u.campaign_id equals c.campain_id where c.campain_id == q1.Id select new Doc_Info { type=u.scoring_id,name=u.file_name }).ToArray();
                q1.Documents_List = q2.ToList();

                var q3 = (from c in db.campaign_score_stage where c.campaign_id == q1.Id select c.campaign_id).FirstOrDefault();
                if (q3 != null)
                {
                    q1.Is_Scoring = true;
                }
                else
                {
                    q1.Is_Scoring = false;
                }

                q1.Category_List = Get_Distinct_Scoring_Category(q1.Id);

                var q4 = (from c in db.campaign_score_stage where c.campaign_id == q1.Id select c.scoring_status).FirstOrDefault();

                q1.Validation_stage = Convert.ToString(q4);

            }

            oresponse.status = 1;
            oresponse.message = "";
            oresponse.content = q1;
            return oresponse;
        }

        public Response Get_Campain_validation_stage(string Campaign_id)
        {
            Response oresponse = new Response();
            var q2 = (from g in db.campaign_validation_stage
                      where g.campaign_id == Campaign_id
                      select new Campaign_Validaton_Details
                      {
                          Campaign_id = g.campaign_id,
                          Isfailure = g.is_failure,
                          validation_message = g.validation_message,
                          validation_status = g.validation_status
                      }).FirstOrDefault();

            oresponse.status = 1;
            oresponse.message = "";
            oresponse.content = q2;
            return oresponse;
        }
        [HttpGet]
        public ScoringData[] Get_Campain_Excel(string Campaign_id)
        {
            List<ScoringData> olist = new List<ScoringData>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
                SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT isnull([CIF],''),isnull([TITLE],''),isnull([HOME_BRANCH],''),isnull([HOST_ID],''),isnull([NATIONALITY],''),isnull([GENDER],''),isnull([BIRTH_DT],''),isnull([CID_BD],''),isnull([AGE],''),isnull([CUST_CREATION_DATE],''),isnull([RELSHIP_YEARS],''),isnull([VIP_CD],''),isnull([VIP_DESC],''),isnull([EMPL_CATEGORY],''),isnull([HAS_CARD],''),isnull([NBR_CARDS],''),isnull([HAS_LOAN],''),isnull([LOANS_ORGAMT],''),isnull([LOANS_VAL],0),isnull([NBR_LOANS],0),isnull([CUST_TYPE],''),isnull([CUST_SEGMENT],''),isnull([PROFESSION],''),isnull([HAS_FUND],''),isnull([NBR_FUNDS],''),isnull([FUND_VAL],''),isnull([CUST_DEP_VAL],0),isnull([CUST_SELECTION],''),isnull([INCOME],0),isnull([prediction],0) FROM [BankingSolution].[dbo].[DSPL_CustomerScoringPrediction_view] where Campaign_Id='"+Campaign_id+"'");

                while (reader.Read())
                {
                    olist.Add(new ScoringData
                    {
                        CIF = reader.GetString(0),
                        TITLE = reader.GetString(1),
                        HOME_BRANCH = reader.GetString(2),
                        HOST_ID = reader.GetString(3),
                        NATIONALITY = reader.GetString(4),
                        GENDER = reader.GetString(5),
                        BIRTH_DT = reader.GetDateTime(6).ToString(),
                        CID_BD = reader.GetDateTime(7).ToString(),
                        AGE = reader.GetString(8),
                        CUST_CREATION_DATE = reader.GetDateTime(9).ToString(),
                        RELSHIP_YEARS = reader.GetString(10),
                        VIP_CD = reader.GetString(11),
                        VIP_DESC = reader.GetString(12),
                        EMPL_CATEGORY = reader.GetString(13),
                        HAS_CARD = reader.GetString(14),
                        NBR_CARDS = reader.GetString(15),
                        HAS_LOAN = reader.GetString(16),
                        LOANS_ORGAMT = reader.GetString(17),
                        LOANS_VAL = reader.GetDecimal(18).ToString(),
                        NBR_LOANS = reader.GetDecimal(19).ToString(),
                        CUST_TYPE = reader.GetString(20),
                        CUST_SEGMENT = reader.GetString(21),
                        PROFESSION = reader.GetString(22),
                        HAS_FUND = reader.GetString(23),
                        NBR_FUNDS = reader.GetString(24),
                        FUND_VAL = reader.GetString(25),
                        CUST_DEP_VAL = reader.GetDecimal(26).ToString(),
                        CUST_SELECTION = reader.GetString(27),
                        INCOME = reader.GetDecimal(28).ToString(),
                        prediction = reader.GetDouble(29).ToString()
                    });
                }
                reader.Close();
                return olist.ToArray();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }
        }

        [HttpGet]
        public Visual_Data[] Get_Scoring_summary(string Campaign_id,string category)
        {
            List<Visual_Data> olist = new List<Visual_Data>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
                SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT [campaignID],[category],[probabilityBucket],[value] FROM [BankingSolution].[dbo].[DSPL_scoringSummaryView] where campaignID='" + Campaign_id + "' and category='"+category+"'");

                while (reader.Read())
                {
                    olist.Add(new Visual_Data(
                        reader.GetString(0),
                        reader.GetString(1),
                        reader.GetString(2),
                        reader.GetString(3)));
                }
                reader.Close();
                return olist.ToArray();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }
        }


        public List<string> Get_Distinct_Scoring_Category(string Campaign_id)
        {
            List<string> olist = new List<string>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
                SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT  distinct [category] FROM [BankingSolution].[dbo].[DSPL_scoringSummaryView] where campaignID='" + Campaign_id + "'");

                while (reader.Read())
                {
                    olist.Add(reader.GetString(0));
                }
                reader.Close();
                return olist;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }
        }


        public cross_sell[] Get_cross_sell(string Campaign_id)
        {
            List<cross_sell> olist = new List<cross_sell>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
                SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT [Month],[Cross_Sell_Percent],[Source],[Destination] FROM [BankingSolution].[dbo].[Cross_Sell_View]");

                while (reader.Read())
                {
                    olist.Add(new cross_sell { month=reader.GetString(0),percent=reader.GetString(1), source=reader.GetString(2),destination=reader.GetString(3)});
                }
                reader.Close();
                return olist.ToArray();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }
        }


        public summary_stats[] Get_Summary_stats(string Campaign_id)
        {
            List<summary_stats> olist = new List<summary_stats>();
            SqlConnection connection = null;
            try
            {
                try
                {
                    connection = Global.GetConnection();
                }
                catch (System.Exception e)
                {
                    Console.Write(e);
                    return null;
                }
                SqlDataReader reader = SqlHelper.ExecuteReader(connection, CommandType.Text, "SELECT [Month],[Trans_Type],[Properties],[PropertiesValue],[RollingMean],[RollingStdDev],[CalcValue] FROM [BankingSolution].[dbo].[Summary_Stats_View]");

                while (reader.Read())
                {
                    olist.Add(new summary_stats {
                        month=reader.GetString(0),
                        calc_value=reader.GetString(1),
                        property=reader.GetString(2),
                        property_value=reader.GetString(3),
                        rolling_mean=reader.GetString(4),
                        rolling_std_dev=reader.GetString(5),
                        trans_type=reader.GetString(6)
                    });
                }
                reader.Close();
                return olist.ToArray();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return null;
            }
            finally
            {
                if (connection != null)
                    connection.Dispose();
            }
        }
    }
}
